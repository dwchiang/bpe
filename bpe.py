import sys
import collections
import bisect
import math
import logging
import re

class SuffixArray(object):
    def __init__(self, data):
        """Naive implementation. I tried something fancier, but because we're
        never interested in patterns that cross line boundaries, this is fine.
        """
        offsets = []
        for li, line in enumerate(data):
            for ci in range(len(line)):
                offsets.append((line[ci:], li, ci))
        offsets.sort()
        self.data = data
        self.offsets = offsets

    def find(self, pattern):
        i = bisect.bisect_left(self.offsets, (pattern,))
        while i < len(self.offsets) and self.offsets[i][0].startswith(pattern):
            _, li, ci = self.offsets[i]
            yield li, ci
            i += 1

class Segmentation(object):
    def __init__(self, line):
        # Essentially a doubly-linked list:
        # next[ci] is the position of the segment after the segment at position ci.
        # prev[ci] is the position of the segment before the segment at position ci.
        self.line = line
        self.next = list(range(1, len(line)))+[None]
        self.prev = [None]+list(range(len(line)-1))

    def at(self, ci):
        if ci is None:
            return None
        elif self.next[ci] is None:
            return self.line[ci:]
        else:
            return self.line[ci:self.next[ci]]

    def merge(self, ci):
        if self.next[ci] is None and self.prev[ci] is None:
            raise ValueError() # not a segment
        if self.prev[ci] is not None:
            self.next[self.prev[ci]] = self.next[ci]
        if self.next[ci] is not None:
            self.prev[self.next[ci]] = self.prev[ci]
        self.next[ci] = None
        self.prev[ci] = None

    def __iter__(self):
        ci = 0
        while ci is not None:
            yield self.line[ci:self.next[ci]]
            ci = self.next[ci]

class SegmentationCounter(object):
    def __init__(self, data):
        self.sa = SuffixArray(data)
        
        # Create initial segmentation
        self.segments = [Segmentation(line) for line in data]

        # Initial counts
        self.c1 = collections.Counter()
        self.c2 = collections.Counter()
        self.n = 0
        for line in data:
            self.n += len(line)
            for a in line:
                self.c1[a] += 1
            for a, b in zip(line, line[1:]):
                self.c2[a, b] += 1

    def merge(self, a, b):
        # Update unigram and bigram counts
        cab = self.c2[a, b]
        self.c1[a] -= cab
        if self.c1[a] == 0: del self.c1[a]
        self.c1[b] -= cab
        if self.c1[b] == 0: del self.c1[b]
        self.c1[a+b] += cab
        self.n -= cab
        del self.c2[a, b]

        # Loop through all occurrences of a+b
        for li, ci in self.sa.find(a+b):
            # Check to make sure that the segmentation is really a b
            if not (self.segments[li].at(ci) == a and
                    self.segments[li].at(ci+len(a)) == b):
                continue

            if self.segments[li].prev[ci] is not None:
                z = self.segments[li].at(self.segments[li].prev[ci])
                if z is not None:
                    self.c2[z, a] -= 1
                    if self.c2[z, a] == 0: del self.c2[z, a]
                    self.c2[z, a+b] += 1
            if self.segments[li].next[ci+len(a)] is not None:
                c = self.segments[li].at(self.segments[li].next[ci+len(a)])
                if c is not None:
                    self.c2[b, c] -= 1
                    if self.c2[b, c] == 0: del self.c2[b, c]
                    self.c2[a+b, c] += 1

            self.segments[li].merge(ci+len(a))

    def bigrams(self):
        return self.c2.keys()

    def count(self, a, b):
        return self.c2[a, b]

    def pmi(self, a, b):
        try:
            return -math.log(self.c1[a] * self.c1[b] / self.c2[a, b] / self.n)
        except (ValueError, ZeroDivisionError):
            return 0.

BOW = "\u300c"
EOW = "\u300d"
        
if __name__ == "__main__":
    if len(sys.argv) < 3:
        exit("usage: bpe.py (learn|apply|unapply) model-file")
    
    cmd = sys.argv[1]
    model = sys.argv[2]

    logging.basicConfig(level=logging.INFO)

    if cmd in ["learn", "apply"]:
        data = []
        for line in sys.stdin:
            words = line.split()
            data.append("".join(BOW+w+EOW for w in words))
        logging.info("precomputing indices")
        sc = SegmentationCounter(data)
        
    elif cmd == "unapply":
        for line in sys.stdin:
            nospace = "".join(line.split())
            words = list(re.findall(BOW+"(.*?)"+EOW, nospace))
            check = "".join(BOW+w+EOW for w in words)
            if nospace != check:
                logging.warning("malformed line: {}".format(line))
            print(" ".join(words))
        exit()

    if cmd == "learn":
        with open(model, "w") as modelfile:
            for oi in range(10):
                best_score = 0.
                best_bigram = None
                for a, b in sc.bigrams():
                    # Only cross word boundaries to merge whole words
                    if a[-1] == EOW and b[0] == BOW:
                        if a[0] != BOW or b[-1] != EOW: continue
                    #score = sc.count(a, b) * sc.pmi(a, b)
                    score = sc.count(a, b)
                    if score > best_score:
                        best_score = score
                        best_bigram = a, b
                a, b = best_bigram
                logging.info("{}. merge {} {} (score={}, tokens={})".format(oi+1, repr(a), repr(b), best_score, sc.n))
                print(a, b, file=modelfile)
                sc.merge(a, b)

    elif cmd == "apply":
        for line in open(model):
            a, b = line.split()
            sc.merge(a, b)
        for line in sc.segments:
            print(" ".join(line))
            
